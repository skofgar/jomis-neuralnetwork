# MLP Neural network

The MLP (multi-layered perceptron) neural network is a simple attempt to explain a neural network through it's visualization. It has been developed as part of a machine learning class assignment at GVSU by [Camila Peñaloza](https://twitter.com/Pilaploza) and [Roland Heusser](https://twitter.com/skofgar).
