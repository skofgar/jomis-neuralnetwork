//////////////////////////
///
///        Base helpers


var print = function (input) {
    console.log(input);
};


var drawTable = function(domId, header, data) {

    var tableHeader = $("#" + domId + " thead");
    tableHeader.empty();

    var tableBody = $("#" + domId + " tbody");
    tableBody.empty();

    var row = "<tr>";
    row += "\n<th>#</th>";
    header.forEach(function(item) {
        row += "\n<th>" + item + "</th>";
    });
    
    row += "\n</tr>";
    tableHeader.append(row);

    for(var rowIndex = 0; rowIndex < data.length; rowIndex++) {
        appendToTable(domId, data[rowIndex]);
    }
}

var appendToTable = function(domId, dataItem) {
    var tableBody = $("#" + domId + " tbody");
    var row = "<tr>";
    row += "<th scope=\"row\">" + $("#" + domId + " tbody tr").length + "</th>";

    dataItem.forEach(function(item) {
        row += "<td>" + item + "</td>";
    });

    row += "</tr>";
    tableBody.append(row);
}
