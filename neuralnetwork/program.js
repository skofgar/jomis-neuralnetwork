var globalNetwork;

//////////////////////////
///
///        Preprocessing

var connectNodes = function (network, layerFrom, layerTo) {
    layerFrom.nodes.forEach( function (fromNode) {
        layerTo.nodes.forEach( function (toNode) {
            var newEdge = new Edge(toNode);
            fromNode.edges.push(newEdge);
            network.links.push({ source: fromNode.name, target: toNode.name, type:"licensing", edge: newEdge });
        });
    });
}

var nodeNameCounter = 0
var configureLayer = function (network, layer, numberOfNodes) {
    for (var i = 0; i < numberOfNodes; i++) {
        layer.nodes.push(new Node(""+nodeNameCounter++));
    }
}

var configureLayerWithNames = function (network, layer, names) {
    for (var i = 0; i < names.length; i++) {
        layer.nodes.push(new Node(names[i]));
    }
}

var configureNetwork = function (network) {
    // choose #of nodes based on input and output values

    // #input nodes = #attributeNames*#attributeNameValues
    var inputNodeNames = [];
    for (var attributeIndex in network.attributes) {
        network.attributeOffsetPosition[attributeIndex] = network.numberOfInputNodes;
        if (network.attributeContinous[attributeIndex]) {
            network.numberOfInputNodes += 1;
            inputNodeNames.push(network.attributes[attributeIndex] + " (continous)");
        } else {
            network.numberOfInputNodes += network.attributeValues[attributeIndex].length;
            network.attributeValues[attributeIndex].forEach( function(attributeValue) {
                print(network.attributes[attributeIndex] + ":" + attributeValue);
                inputNodeNames.push(network.attributes[attributeIndex] + ":" + attributeValue);
            });
        }
    }

    // #output nodes = #targetValues
    network.numberOfOutputNodes = network.targetValues.length;

    // #hidden layer nodes = 2/3 (#input + #output)
    network.numberOfHiddenNodes = Math.ceil(2.0 / 3.0 * (network.numberOfInputNodes + network.numberOfOutputNodes));

    var debug = false;
    if (debug) {
        setupInClassExample(network);
    } else {
        var hiddenNames = [];
        for (var i = 0; i < network.numberOfHiddenNodes; i++) {
            hiddenNames.push("h"+i);
        }

        // configure layers
        configureLayerWithNames(network, network.input, inputNodeNames);
        configureLayerWithNames(network, network.output, network.targetValues);
        configureLayerWithNames(network, network.hidden, hiddenNames);

        connectNodes(network, network.input, network.hidden);
        connectNodes(network, network.hidden, network.output);
    }

    if (network.shuffleTrainingData) {
        shuffleTrainingData(network.trainingData);
    }

    network.verificationData = network.trainingData.splice(0, network.trainingData.length * network.verificationTrainingSplit);

    for (var i = 0; i < network.trainingData.length; i++) {
        network.trainingData[i]
    }
}

var setupInClassExample = function (network) {

    // class example
    configureLayer(network, network.input, 2);
    configureLayer(network, network.output, 1);
    configureLayer(network, network.hidden, 2);

    connectNodes(network, network.input, network.hidden);
    connectNodes(network, network.hidden, network.output);

    network.input.nodes[0].edges[0].weight = 1.0;
    network.input.nodes[0].edges[1].weight = -1.0;

    network.input.nodes[1].edges[0].weight = 0.5;
    network.input.nodes[1].edges[1].weight = 2.0;

    network.hidden.nodes[0].edges[0].weight = 1.5;
    network.hidden.nodes[1].edges[0].weight = -1.0;

    network.targetValues = [1];
    network.attributes = ["Health", "Armor"];
    network.attributeOffsetPosition = [0, 1]; // what is the relative position (all previous ones accumulated)
    network.attributeContinous = [false, false];
    network.attributeValues = [[1,0],[1,0]];

    network.trainingData = [
        [0, 1, 1],
        [0, 1, 1]
    ];
}


//////////////////////////
///
///        Set Inputs

var attributeValueInputNodeIndex = function (network, attributeIndex, attributeValue) {
    var isContinous = network.attributeContinous[attributeIndex];
    var nodeIndex = network.attributeOffsetPosition[attributeIndex]
    if (!isContinous) {
        nodeIndex += network.attributeValues[attributeIndex].indexOf(attributeValue);
    }
    return nodeIndex;
}


var processSample = function (network, sample) {

    for (var i = 0; i < network.input.nodes.length; i++) {
        network.input.nodes[i].input = 0.0;
    }

    for (var i = 0; i < sample.length-1; i++) { // -1 to remove target
        var inputNodeIndex = attributeValueInputNodeIndex(network, i, sample[i]);
        var isContinous = network.attributeContinous[i];

        if (isContinous) {
            network.input.nodes[inputNodeIndex].input = parseFloat(sample[i]);
        } else {
            network.input.nodes[inputNodeIndex].input = 1.0;
        }
    }
    if ("online" === network.trainingMode){
        sendProductToChildren(network.input, network.hidden);
        sendProductToChildren(network.hidden, network.output);

    }
}


//////////////////////////
///
///        Compute Layer Update

var sendProductToChildren = function (fromLayer, toLayer) {
    fromLayer.nodes.forEach( function (node) {
        node.edges.forEach( function (edge) {
            var product = edge.weight * node.input;
            edge.target.parentProduct.push(product);
        });
    });
    computeSumOfProducts(toLayer);
}

var sigmoid = function (t) {
    return 1/(1+Math.pow(Math.E, -t));
}

var computeSumOfProducts = function (layer) {
    layer.nodes.forEach( function (node) {
        var sumOfProducts = node.bias + node.parentProduct.reduce(function(a, b) {
            return a + b;
        });
        node.parentProduct = [];
        node.input = sigmoid(sumOfProducts);
    });
}

// for online testing only
var computeNetworkError = function (network, sample) {
    var targetValue = sample[sample.length-1];
    var indexOfTargetValue = network.targetValues.indexOf(targetValue);
    var networkError = 0.0;

    for (var i = 0; i < network.targetValues.length; i++) {
        if (i === indexOfTargetValue) {
            networkError += Math.pow(Math.abs(network.output.nodes[i].input - 1.0),2);
            network.output.nodes[i].target = 1.0;
        } else {
            networkError += Math.pow(Math.abs(network.output.nodes[i].input - 0.0),2);
            network.output.nodes[i].target = 0.0;
        }
    }

    networkError /= 2.0;
    network.globalError = networkError;
}

var computeErrorOutputNodes = function (network) {
    network.output.nodes.forEach( function(node) {
        node.error = node.input * (1 - node.input) * (node.target - node.input);
        node.bias = node.bias + network.learningFactor * node.error * node.bias;
    });
}

var computeErrorHiddenNodes = function (network) {
    network.hidden.nodes.forEach( function(node) {
        var sumProduct = 0;
        node.edges.forEach( function(edge) {
            sumProduct += edge.weight * edge.target.error;
        });
        node.error = node.input * (1 - node.input) * sumProduct;
        node.bias = node.bias + network.learningFactor * node.error * node.bias;
    });
}

var updateWeights = function (network, layer) {
    layer.nodes.forEach( function (node) {
        node.edges.forEach( function (edge) {
            edge.weight = edge.weight + network.learningFactor * edge.target.error * node.input;
        });
    });
}

//////////////////////////
///
///        Prediction


var prediction = function (network, trainingSample) {
    var highestProbability = 0.0;
    var highestProbabilityNodeIndex = 0;

    for (var i = 0; i < network.output.nodes.length; i++) {
        var node = network.output.nodes[i];
        if (node.input > highestProbability) {
            highestProbability = node.input;
            highestProbabilityNodeIndex = i;
        }
    }
    return { target: network.targetValues[highestProbabilityNodeIndex], probability: highestProbability };
}



//////////////////////////
///
///        User Sample Prediction


var createUserInput = function (network) {
    $("#userInput").attr("placeholder", network.trainingData[0].slice(0,network.trainingData[0].length-1));

}

//////////////////////////
///
///        Training Data Modifications

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 * Source: http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
 */
var shuffleTrainingData = function (array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

//////////////////////////
///
///        Draw network

var drawNetwork = function(network) {
    redraw(network);
}


//////////////////////////
///
///        Training Loop


var learningIteration = function (network) {
    network.trainingData.forEach( function (trainingSample) {
        processSample(network, trainingSample);
        computeNetworkError(network, trainingSample);
        computeErrorOutputNodes(network);
        computeErrorHiddenNodes(network);
        updateWeights(network, network.hidden);
        updateWeights(network, network.input);


    });

    var verificationSampleCorrectCount = 0;
    network.verificationData.forEach( function (verificationSample) {
        processSample(network, verificationSample);
        var predictionResult = prediction(network, verificationSample);
        if (verificationSample[network.attributes.length] === predictionResult.target) {
            verificationSampleCorrectCount++;
        }
    });

    network.verificationErrorRateArray.push(1.0-(parseFloat(verificationSampleCorrectCount)/network.verificationData.length));
    network.globalErrorArray.push(network.globalError);
    network.epoch++;
}

var mainLoop = function (network) {
    parametrizeNetwork(network);

    // for loop #epoch / #time
    for (var i = 0; i <= network.totalNumberOfEpochs; i++) {
        setTimeout(learningIteration(network), 2035000);
    }
    clearChart();
    drawChart({ globalError: network.globalErrorArray, classificationError: network.verificationErrorRateArray });
    drawNetwork(network);
    drawTable("userVerificationHistoryTable", network.attributes.concat("Target"), []);
    $(".result").show();
    globalNetwork = network;
    createUserInput(network);
}

var networkStart = function(data) {
    var network = new Network();
    processFileData(data, network);
    configureNetwork(network);
    mainLoop(network);
}
