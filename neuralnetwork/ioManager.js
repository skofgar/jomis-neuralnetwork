//////////////////////////
///
///        IO STUFF

var networkStartFromFile = function(file) {
    var network = new Network();
    readInputFile(file, network);
}

var readInputFile = function (input, network) {
    var files = input.target.files;

    var reader = new FileReader();
    reader.onload = function (progressEvent){
        processFileData (progressEvent.target.result, network);
        configureNetwork(network);
        mainLoop(network);
    };
    reader.readAsText (files[0]);
}

var processFileData = function (input, network) {

	// By lines
	var lines = input.split('\n');

    for(var lineIndex = 0; lineIndex < lines.length; lineIndex++){
        var line = lines[lineIndex];
        var tokensWithEmptyStrings = line.split(/[\s\r]+/);
        var tokens = tokensWithEmptyStrings.filter(function(v){return v!==''});

        if (tokens.length > 1) {
            if (0 === lineIndex) {
                parseHeaderLine(network, tokens);
            } else {
                parseLine(network, tokens);
                network.trainingData.push(tokens);
            }
        }
	}
    //network.trainingData = lines.splice(1,lines.length-1);
}

var parseHeaderLine = function (network, tokens) {
    var targetIndex = tokens.length-1;
    for (var attributeNameIndex in tokens) {
        if (attributeNameIndex === (targetIndex+"")) {
            // do nothing
        } else {
            network.attributes.push(tokens[attributeNameIndex]);
            network.attributeValues.push([]);
            network.attributeContinous.push(true);
        }
    }
}

var parseLine = function (network, tokens) {
    var targetIndex = tokens.length-1;

    for (var attributeIndex in tokens) {
        if (attributeIndex === (targetIndex+"")) {
            addToTargetValues(network, tokens[attributeIndex]);
        } else {
            addToAttributeValues(network, attributeIndex, tokens[attributeIndex]);
        }
    }
}

var addToTargetValues = function (network, value) {
    if (-1 === network.targetValues.indexOf(value)) {
        network.targetValues.push(value);
    }
}

var addToAttributeValues = function (network, attributeIndex, value) {
    if (-1 === network.attributeValues[attributeIndex].indexOf(value)) {
        network.attributeValues[attributeIndex].push(value);

        if (isNaN(value)) {
            network.attributeContinous[attributeIndex] = false;
        }
    }
}

var parametrizeNetwork = function (network) {

  if ($('#learning-factor').val() != null && $('#learning-factor').val() != '' && $('#learning-factor').val() > 0) {
    network.learningFactor = parseFloat($("#learning-factor").val());
  }

  if ($('#epochs').val() != null && $('#epochs').val() != '' && $('#epochs').val() > 0) {
    network.totalNumberOfEpochs = parseInt($('#epochs').val());
  }

  if ($('input:radio[name=learning-options]')[0].checked = true){
    network.trainingMode = "online";
  } else if ($('input:radio[name=learning-options]')[1].checked = true) {
    network.trainingMode = "batch";
  }

  if($('#shuffle-training').is(':checked')){
    network.shuffleTrainingData = true;
  } else {
    network.shuffleTrainingData = false;
  }

  if (isNaN($('#percentage-training').val())){
    $('#percentage-validation').val("");
    $('#percentage-training').val("");
  } else {
    network.verificationTrainingSplit = parseInt($("#percentage-validation").val()) / 100;
  }
}

$('#percentage-training').change(function () {
  if (isNaN(parseInt($('#percentage-training').val())) || parseInt($('#percentage-training').val())<1) {
    $('#percentage-validation').val("30");
    $('#percentage-training').val("70");
  } else {
    $('#percentage-validation').val("" + 100-$('#percentage-training').val());
  }
})
.change();

$('#percentage-validation').change(function () {
  if (isNaN(parseInt($('#percentage-validation').val())) || parseInt($('#percentage-validation').val())<0) {
    $('#percentage-validation').val("30");
    $('#percentage-training').val("70");
  } else {
    $('#percentage-training').val("" + 100-$('#percentage-validation').val());
  }
})
.change();
