//////////////////////////
///
///        Load Stuff

$("#gameExample").click(function () {
    $.get("neuralnetwork/examples/game.csv", function (data) {
        networkStart(data);
    });
});

$("#fishingExample").click(function () {
    $.get("neuralnetwork/examples/fishing.csv", function (data) {
        networkStart(data);
    });
});

$("#flowerExample").click(function () {
    $.get("neuralnetwork/examples/irisData2.csv", function (data) {
        networkStart(data);
    });
});

$('#files').change(function (selectedFiles) {
    networkStartFromFile(selectedFiles);
});

$('#checkUserInput').click(function () {
    var input = $("#userInput").val();
    input = input.replace(/\s+/g, '');
    input = input.split(",");

    if (input.length === globalNetwork.attributes.length) {
        var inputWithTarget = input.concat("");
        processSample(globalNetwork, inputWithTarget);
        var predictionResult = prediction(globalNetwork, input);

        var tableEntry = input.concat(predictionResult.target);
        tableEntry.push(predictionResult.probability);
        appendToTable("userVerificationHistoryTable", tableEntry);
    } else {
        alert("Please make sure you add " + globalNetwork.attributes.length + " attribute-values. (Only " + input.length + " read.)");
        return;
    }
});
