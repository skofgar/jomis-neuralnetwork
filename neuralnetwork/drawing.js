var colorRed      = "#c9302c";
var colorBlue     = "#286090";
var colorGreen    = "#449d44";
var colorLightBlue= "#31b0d5";
var colorYellow   = "#ec971f";

var clearChart = function () {
    $("#networkError").empty();
}


var errorChart = {};
var drawChart = function (data) {
    // define dimensions of graph
	var m = [80, 80, 80, 80]; // margins
	var w = 1000 - m[1] - m[3]; // width
	var h = 600 - m[0] - m[2]; // height
    // w = $("svg").width();
    // h = $("svg").height();

	// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
    var numberOfEpochs = data.globalError.length

    var maxValue = 0.0;
    data.globalError.forEach( function(value) {
        if (value > maxValue) {
            maxValue = value;
        }
    });
    data.classificationError.forEach( function(value) {
        if (value > maxValue) {
            maxValue = value;
        }
    });

	// X scale will fit all values from data[] within pixels 0-w
	var x = d3.scale.linear().domain([0, numberOfEpochs]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
	var y = d3.scale.linear().domain([0, maxValue]).range([h, 0]);
		// automatically determining max range can work something like this
		// var y = d3.scale.linear().domain([0, d3.max(data)]).range([h, 0]);
	// create a line function that can convert data[] into x and y points
	var line = d3.svg.line()
		// assign the X function to plot our line as we wish
    	.x(function(d,i) {
    		// verbose logging to show what's actually being done
    		// console.log('Plotting X value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
    		// return the X coordinate where we want to plot this datapoint
    		return x(i);
    	})
    	.y(function(d) {
    		// verbose logging to show what's actually being done
    		// console.log('Plotting Y value for data point: ' + d + ' to be at: ' + y(d) + " using our yScale.");
    		// return the Y coordinate where we want to plot this datapoint
    		return y(d);
    	})
	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#networkError") //.append("svg:svg")
	      .attr("width", w + m[1] + m[3])
	      .attr("height", h + m[0] + m[2])
	    .append("svg:g")
	      .attr("transform", "translate(" + m[3] + "," + m[0] + ")");
	// create yAxis
	var xAxis = d3.svg.axis().scale(x).tickSize(-h).tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + h + ")")
	      .call(xAxis);
	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y).ticks(4).orient("left");
	// Add the y-axis to the left
	graph.append("svg:g")
	      .attr("class", "y axis")
	      .attr("transform", "translate(-25,0)")
	      .call(yAxisLeft);

		// Add the line by appending an svg:path element with the data line we created above
	// do this AFTER the axes above so that the line is above the tick-lines


    // data.forEach( function(lineData) {
    //
    // });
    graph.append("svg:path")
        .attr("d", line(data.globalError))
        .attr("data-legend", function(d) { return "Global Error"})
        .attr("stroke", colorBlue);
    graph.append("svg:path")
        .attr("d", line(data.classificationError))
        .attr("data-legend", function(d) { return "Classification Error"})
        .attr("stroke", colorRed);

    var textLength = 40;
    var textVerticalOffset = -10;
    graph.append("text")
		.attr("x", (x(data.globalError.length)+3))
        .attr("y", y(data.globalError[data.globalError.length-1]))
		.attr("dy", ".35em")
		.attr("text-anchor", "start")
		.style("fill", colorBlue)
		.text("Global Error");

	graph.append("text")
		.attr("transform", "translate(" + (x(data.globalError.length)+3) + "," + y(data.classificationError[data.globalError.length-1]) + ")")
		.attr("dy", ".35em")
		.attr("text-anchor", "start")
		.style("fill", colorRed)
		.text("Classification Error");


}





//////////////////////////
///
///        d3js.org Drawing Graph

var pushNode = function (layerSource, layerTarget, xSource, xTarget, height, links) {
    var sourceVerticalOffsetSize = height / (layerSource.nodes.length);
    var targetVerticalOffsetSize = height / (layerTarget.nodes.length);
    var tempSourceY = sourceVerticalOffsetSize;

    layerSource.nodes.forEach( function(fromNode) {
        tempTargetY = targetVerticalOffsetSize;
        for (var edgeIndex in fromNode.edges) {
            var targetEdge = fromNode.edges[edgeIndex];
            links.push({ source: fromNode.name, target: targetEdge.target.name, type:"licensing"
                        , edge: targetEdge, xSource: xSource, ySource: tempSourceY
                        , xTarget: xTarget, yTarget: tempTargetY });
            tempTargetY += targetVerticalOffsetSize;
        }
        tempSourceY += sourceVerticalOffsetSize;
    });
}

var redraw = function(network) {
    var m = [80, 80, 80, 80]; // margins
    var width = 1024,
        height = 600;

    var horizontalOffsetSize = width / 3;
    var padding = horizontalOffsetSize/2;

    var links = [];
    var nodes = {};

    pushNode(network.input, network.hidden, padding, padding+horizontalOffsetSize, height, links);
    pushNode(network.hidden, network.output, padding+horizontalOffsetSize,  padding+horizontalOffsetSize*2, height, links);

    // Compute the distinct nodes from the links.
    links.forEach(function(link) {
      link.source = nodes[link.source] || (nodes[link.source] = {name: link.source, fixed: true, x: link.xSource, y: link.ySource});
      link.target = nodes[link.target] || (nodes[link.target] = {name: link.target, fixed: true, x: link.xTarget, y: link.yTarget});
    });

    var force = d3.layout.force()
        .nodes(d3.values(nodes))
        .links(links)
        .size([width, height])
        .linkDistance(300)
        .charge(-200)
        .on("tick", tick)
        .start();

    var drag = force.drag()
    .on("dragstart", dragstart);

    /*var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height);*/
    $("#networkGraph").empty();
    var svg = d3.select("#networkGraph");

    svg.attr("width", width + m[1] + m[3])
        .attr("height", height + m[0] + m[2]);

    // Per-type markers, as they don't inherit styles.
    svg.append("defs").selectAll("marker")
        .data(["suit", "licensing", "resolved"])
      .enter().append("marker")
        .attr("id", function(d) { return d; })
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 15)
        .attr("refY", -1.5)
        .attr("markerWidth", 6)
        .attr("markerHeight", 6)
        .attr("orient", "auto")
      .append("path")
        .attr("d", "M0,-5L10,0L0,5");

    var path = svg.append("g").selectAll("path")
        .data(force.links())
      .enter().append("path")
        .attr('stroke-width', function(d) {
            //debugger;
            //print(d);
            return Math.abs(d.edge.weight); })
        .attr('stroke', function(d) {
            print("weight: " + d.edge.weight + " color: " + (d.edge.weight > 0.0) ? colorGreen : colorRed);
            return (d.edge.weight > 0) ? colorGreen : colorRed;
        })
        .attr("class", function(d) { return "networkPath " + fixNameForCSS(d.source.name) + " " + fixNameForCSS(d.target.name) })
        .attr("marker-end", function(d) { return "url(#" + d.type + ")"; });

    var circle = svg.append("g").selectAll("circle")
        .data(force.nodes())
      .enter().append("circle")
        .attr("r", 6)
        //.call(force.drag);
        .on("dblclick", dblclick)
        .on("mouseover", circleHover)
        .on("mouseout", circleUnhover)
        .call(drag);

    var text = svg.append("g").selectAll("text")
        .data(force.nodes())
      .enter().append("text")
        .attr("x", 8)
        .attr("y", ".31em")
        .text(function(d) { return d.name; });

    // Use elliptical arc path segments to doubly-encode directionality.
    function tick() {
      path.attr("d", linkArc);
      circle.attr("transform", transform);
      text.attr("transform", transform);
    }

    function linkArc(d) {
      var dx = d.target.x - d.source.x,
          dy = d.target.y - d.source.y,
          dr = Math.sqrt(dx * dx + dy * dy);
      return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
    }

    function dblclick(d) {
        d3.select(this).classed("fixed", d.fixed = false);
    }

    function dragstart(d) {
        d3.select(this).classed("fixed",d.fixed = true);
    }

    function circleHover(d) {
        var className = fixNameForCSS(d.name);
        $(".networkPath:not(." + className + ")").fadeTo(0.2,0.2);
    }

    function circleUnhover(d) {
        $(".networkPath").fadeTo(0.2,1);
    }

    function fixNameForCSS(stringToFix) {
        return stringToFix.replace(":", "");
    }

    function transform(d) {
      return "translate(" + d.x + "," + d.y + ")";
    }

}
