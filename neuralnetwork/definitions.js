function Node(name) {
    this.name = name;
    this.edges = [];
    this.error = 0.0;
    this.target = 0.0;
    this.bias = 1.0;
    this.input = 0.0;
    this.parentProduct = [];
};

function Edge(targetNode) {
    this.target = targetNode;
    this.weight = Math.random() * (0.1 - (-0.1)) -0.1; //Math.random();         //(Math.random()-0.5)/5.0
};

function Layer() {
    this.nodes = [];
};

function Network() {
    this.epoch = 0;
    this.targetValues = [];
    this.attributes = [];
    this.attributeOffsetPosition = []; // what is the relative position (all previous ones accumulated)
    this.attributeContinous = [];
    this.attributeValues = [];

    this.trainingData = [];
    this.verificationData = [];
    this.shuffleTrainingData = true;
    this.totalNumberOfEpochs = 500;
    this.verificationTrainingSplit = 0.3;
    this.verificationErrorRateArray = [];

    this.trainingMode = "online"; // or "batch"

    this.numberOfInputNodes = 0;
    this.numberOfOutputNodes = 0;
    this.numberOfHiddenNodes = 0;

    this.input = new Layer();
    this.hidden = new Layer();
    this.output = new Layer();

    this.globalError = 0.0;
    this.globalErrorArray = [];
    this.learningFactor = 0.05;

    this.links = [];
    this.nodes = {};
};
